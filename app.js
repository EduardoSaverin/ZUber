const express = require('express');
const app = express();
const routes = require('./routes/routes');
const port = require('./port');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
// If we are running mocha tests then it will set APP_ENVIORNMENT to test
// Then we will change database for testing purpose.
// Testing connection is made in test_helper.js
if(process.env.APP_ENVIORNMENT !== 'test'){
    mongoose.connect('mongodb://localhost/zuber',{useMongoClient:true});
}

mongoose.Promise = global.Promise;

app.use(bodyParser.json())
routes(app);
// Error Handler Middleware
app.use((error,request,response,next)=> {
    response.status(422).send({error:error.message});
});
port(app);

module.exports = app; 