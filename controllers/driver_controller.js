const Driver = require('../models/driver');

// These are just functions to handle GET,POST requests.
// See routes.js for real use
module.exports = {
    greeting(request, response) {
        response.send({hi: 'there'});
    },
    create(request,response,next) {
        Driver.create(request.body)
        .then(driver => response.send(driver))
        .catch(next);
    },
    edit(request,response,next){
        Driver.findByIdAndUpdate({_id: request.params.id},request.body)
        .then(() => Driver.findById({_id: request.params.id}))
        .then((driver) => response.send(driver))
        .catch(next);
    },
    delete(request,response,next){
        Driver.findByIdAndRemove({_id: request.params.id})
        .then(driver => response.status(201).send(driver))
        .catch(next);
    },
    index(request,response,next){
        const {lng,lat} = request.query;
        console.log(lng,lat);
        // Here first parameter in geoNear is called `near`
        // Second is option
        // This `drivers.ensureIndex({ 'geometry.coordinates' : '2dsphere'}))` is neccessary in driver_controller_test.js
        Driver.geoNear(
            {type: 'Point',coordinates: [parseFloat(lng),parseFloat(lat)]},
            {spherical: true,maxDistance: 500000}
        )
        .then(drivers => response.send(drivers))
        .catch(next);
    }
}