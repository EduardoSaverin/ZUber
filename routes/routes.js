const DriverController = require('../controllers/driver_controller');

module.exports = (app) => {
    // Handling GET request to http://localhost:3000/api/drivers
    app.get('/api/drivers',DriverController.index);

    // POST request to create Driver in database
    app.post('/api/drivers',DriverController.create);

    // PUT request to update driver details
    app.put('/api/drivers/:id',DriverController.edit);

    // DELETE request to remove driver
    app.delete('/api/drivers/:id',DriverController.delete);
};