const assert = require('assert');
// Supertest is a library for tesing HTTP connections.
const request = require('supertest');
const app = require('../app');
const mongoose = require('mongoose');
const Driver = mongoose.model('driver');

beforeEach(done => {
    const {
        drivers
    } = mongoose.connection.collections;
    drivers.drop()
        .then(() => drivers.ensureIndex({ 'geometry.coordinates' : '2dsphere'}))
        .then(() => done())
        .catch(() => done());
});

describe('Driver Controller Test', () => {

    it('POST to /api/drivers creates a new driver', (done) => {
        Driver.count().then(count => {
            request(app)
                .post('/api/drivers')
                .send({
                    email: 'test@test.com',
                    driving: true
                })
                .end((error, response) => {
                    Driver.count().then(newCount => {
                        assert(count + 1 === newCount);
                        done();
                    });
                });
        });

    });
    it('PUT to /api/drivers/:id updates the driver', (done) => {
        const driver = new Driver({email:'driver@gmail.com',driving:false});
        driver.save()
        .then(() => {
            request(app)
            .put('/api/drivers/'+driver._id)
            .send({
                driving: true
            })
            .end((error, response) => {
                Driver.findById({_id: driver._id})
                .then((driver) => {
                    assert(driver.driving === true);
                    done();
                });
            });
        });
    });
    it('DELETE to /api/drivers/:id deletes driver',(done) => {                        
        const driver = new Driver({email:'driver_delete@gmail.com',driving:false});
        driver.save()
        .then(() => {
            request(app)
            .delete('/api/drivers/'+driver._id)
            .end((error,response) => {
                Driver.findById({_id: driver._id})
                .then(driver => {
                    assert(driver === null);
                    done();
                });
            });
        })
    });
    it('GET to /api/drivers to find drivers',(done) => {
        const uttarPradeshDriver = new Driver({
            email : 'noida@gmail.com',
            driving: true,
            geometry : {type:'Point',coordinates: [-122.23443,45.565756]}
        });
        const delhiDriver = new Driver({
            email : 'delhi@gmail.com',
            driving: false,
            geometry : {type :'Point' , coordinates: [-12.345664,25.420678]}
        });
        Promise.all([uttarPradeshDriver.save(),delhiDriver.save()])
        .then(() => {
            request(app)
            .get('/api/drivers?lng=-12&lat=25')
            .end((error,response) => {
                assert(response.body[0].obj.email === 'delhi@gmail.com');
                console.log(response);
                done();
            });
        });
    });
});